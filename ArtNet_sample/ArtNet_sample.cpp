#include "ArtNet_sample.h"
#include "sfx_msg.h"


ArtNet_sample::ArtNet_sample(QWidget *parent)
    : QMainWindow(parent)
{
    ui.setupUi(this);
	sender_timerid = 0;

	send_socket = new QUdpSocket();
	ui.udp_s_btn->setText("START");
	connect(ui.udp_s_btn, SIGNAL(released()), SLOT(clicked_start_stop_btn()));

}

ArtNet_sample::~ArtNet_sample()
{
	if (sender_timerid != 0) {
		killTimer(sender_timerid);
		sender_timerid = 0;
	}
	
	delete send_socket;
	send_socket = NULL;
}

void ArtNet_sample::clicked_start_stop_btn() {
	if (sender_timerid == 0) {
		sender_timerid = startTimer(23); // DMX max fresh rate
		ui.udp_s_btn->setText("STOP");
		ui.ip_edit->setEnabled(false);
	}else{
		ui.udp_s_btn->setText("START");
		ui.ip_edit->setEnabled(true);
	}
	
}

void ArtNet_sample::timerEvent(QTimerEvent * event)
{
	if (event->timerId() == sender_timerid) {
		send_artdmx_data();
	}

}


void ArtNet_sample::send_artdmx_data()
{
	ArtDmx dmx;
	memset(&dmx, 0, sizeof(ArtDmx));
	memcpy(dmx.id, ART_NET, sizeof(dmx.id));
	dmx.opCode = 0x5000;
	dmx.protverHi = 0;
	dmx.protverLo = 14;
	dmx.seq = 0; 
	dmx.physical = 0; 
	dmx.subUni = ui.uni_edit->text().toInt();
	dmx.net = 0;
	dmx.lengthHi = (DMX_MAX_DATA_NUM >> 8) & 0xFF;
	dmx.lengthLo = (DMX_MAX_DATA_NUM & 0xFF);
	
	
	int ch_1 = ui.ch_edit_1->text().toInt();
	int ch_2 = ui.ch_edit_2->text().toInt();
	int ch_3 = ui.ch_edit_3->text().toInt();

	uint8_t value_1 = (uint8_t)ui.slider_1->value();
	uint8_t value_2 = (uint8_t)ui.slider_2->value();
	uint8_t value_3 = (uint8_t)ui.slider_3->value();

	dmx.data[ch_1-1] = value_1;
	dmx.data[ch_2-1] = value_2;
	dmx.data[ch_3-1] = value_3;

	QString ip = ui.ip_edit->text();
	send_socket->writeDatagram((const char*)&dmx, sizeof(ArtDmx), QHostAddress(ip), ART_NET_PORT);


}


