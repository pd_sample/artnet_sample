#pragma once

#include <QtWidgets/QMainWindow>
#include <QEvent>
#include <QUdpSocket>
#include "ui_ArtNet_sample.h"



class ArtNet_sample : public QMainWindow
{
    Q_OBJECT

public:
    ArtNet_sample(QWidget *parent = Q_NULLPTR);
	~ArtNet_sample();

protected:
	virtual void timerEvent(QTimerEvent *event);
private:
    Ui::ArtNet_sampleClass ui;
	int sender_timerid;

//## �إ�QT UDP Socket
	QUdpSocket* send_socket;

	private Q_SLOTS:

	void clicked_start_stop_btn();
	void send_artdmx_data();

};
