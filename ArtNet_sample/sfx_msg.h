#ifndef SFX_MSG_H_
#define SFX_MSG_H_

#define ART_NET  ("Art-Net\0")
#define ART_NET_PORT 6454
#define DMX_MAX_DATA_NUM 512

typedef struct {
    char		id[8];      //"Art-Net"
    uint16_t	opCode;     // See Doc. Table 1 - OpCodes eg. 0x5000 OpOutput / OpDmx
	uint8_t     protverHi;  // High byte of the Art-Net protocol revision number.
	uint8_t     protverLo;  // Low byte of the Art-Net protocol revision number.Current value 14.
    uint8_t		seq;        // The Sequence field is set to 0x00 to disable this feature.
    uint8_t		physical;   // The physical input port from which DMX512 data was input. This field is for information only.
    uint8_t		subUni;     // The low byte of the 15 bit Port-Address to which this packet is destined
    uint8_t		net;        // The top 7 bits of the 15 bit Port-Address to which this packet is destined.
    uint8_t		lengthHi;   // data length (2 - 512)
    uint8_t		lengthLo;   //
    uint8_t		data[512];	// universe data
} ArtDmx;


#endif // SFX_MSG_H_
